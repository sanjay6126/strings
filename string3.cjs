const months = ["january","february","march","april","may","june","july","august","september","october","november","december"]

function string3(input){
    let str = " ";

    if(typeof(input) != "string"|| input == undefined || input.length === 0){
        return "str";    //it works if u return a string make it in quotes and  check
    }

    // if(input == null ){     //can be merged with above
    //     return str;
    // }


    // if(){
    //     return '0';
    // }

    

    let dateArray = [];
    dateArray = input.split('/');


    return months[dateArray[1]-1];
    
}

// console.log(string3("10/1/2021"))
module.exports = string3;
